# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="wondershaper is a QoS script"
HOMEPAGE="http://lartc.org/wondershaper"
SRC_URI="http://lartc.org/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"

KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="sys-apps/iproute2"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	cd "${S}"
	epatch "${FILESDIR}"/${P}-gentoo.patch
}

src_compile() {

	mv wshaper wshaper.cbq

	for i in wshaper*; do
		SCRIPT=${i/wshaper/wondershaper}
		awk '/^if \[/{firstPartOver=1}{if (firstPartOver) print $0}' ${i} \
		| awk '                 {addOR=nextAddOR; nextAddOR=0}
				/tc.*add/       {addOR=1}
                /\\$/           {nextAddOR=addOR;addOR=0}
                                {printf("%s",$0);
                if (addOR) print " || return 1"
                else printf "\n"}' \
		| sed 's/exit/return 0/' > ${SCRIPT}
	done
}

src_install() {
	dosbin wondershaper.cbq wondershaper.htb || die
	newsbin "${FILESDIR}"/wondershaper-1.1a.gentoo wondershaper || die
	dodir /etc/wondershaper || die
	insinto /etc/wondershaper || die
	doins "${FILESDIR}"/wondershaper.conf || die
	newinitd "${FILESDIR}"/wondershaper.rc wondershaper || die
	newconfd "${FILESDIR}"/wondershaper.confd wondershaper || die
	dodoc ChangeLog README TODO COPYING VERSION || die
}
