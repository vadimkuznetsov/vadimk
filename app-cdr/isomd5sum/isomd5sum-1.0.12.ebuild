# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="isomd5sum is implanting a MD5 checksum in an ISO"
HOMEPAGE="http://fedorahosted.org/releases/i/s/isomd5sum"
SRC_URI="https://git.fedorahosted.org/cgit/isomd5sum.git/snapshot/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
