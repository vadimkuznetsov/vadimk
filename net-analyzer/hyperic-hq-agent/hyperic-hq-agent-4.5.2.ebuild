# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit eutils

DESCRIPTION="Hyperic is application monitoring and performance management for
virtual, physical, and cloud infrastructures."
HOMEPAGE="http://www.hyperic.com/"
SRC_URI="mirror://sourceforge/project/hyperic-hq/Hyperic%20${PV}/Hyperic%20${PV}-GA/${P}-noJRE.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="mysql"

DEPEND="=virtual/jre-1.6.0
	mysql? ( dev-java/jdbc-mysql )"
RDEPEND="${DEPEND}"

pkg_setup() {
	HQ_GROUP=${HQ_GROUP:-hyperic}
	HQ_USER=${HQ_USER:-hyperic}
	HQ_INSTALL_DIR="/opt/hyperic-hq/agent"

	enewgroup ${HQ_GROUP}
	enewuser ${HQ_USER} -1 "/bin/bash" -1 ${HQ_GROUP}
}

src_prepare() {
	find "${S}" -name "*.bat" -delete || die
	find "${S}" -name "*.vbs" -delete || die
	find "${S}" -name "*.dll" -delete || die
	find "${S}" -name "*.exe" -delete || die
	find "${S}" -name "*.a" -delete || die
	find "${S}" -name "*.sl" -delete || die
	find "${S}" -name "*.jnilib" -delete || die
	find "${S}"/wrapper -name "*aix*" -delete || die
	find "${S}"/wrapper -name "*freebsd*" -delete || die
	find "${S}"/wrapper -name "*hpux*" -delete || die
	find "${S}"/wrapper -name "*macosx*" -delete || die
	find "${S}"/wrapper -name "*solaris*" -delete || die
	find "${S}"/wrapper -name "*ppc*" -delete || die
	find "${S}"/bundles/agent-${PV}/pdk/lib -name "*ia64*" -delete || die
	find "${S}"/bundles/agent-${PV}/pdk/lib -name "*freebsd*" -delete || die
	find "${S}"/bundles/agent-${PV}/pdk/lib -name "*s390*" -delete || die
	find "${S}"/bundles/agent-${PV}/pdk/lib -name "*solaris*" -delete || die
	find "${S}"/bundles/agent-${PV}/pdk/lib -name "*ppc*" -delete || die
}

src_install() {
	into "${HQ_INSTALL_DIR}"
	#dobin bin/*

	insinto "${HQ_INSTALL_DIR}"/conf
	doins conf/*

	insinto "${HQ_INSTALL_DIR}"/wrapper/lib
	doins wrapper/lib/wrapper.jar

	into "${HQ_INSTALL_DIR}/bundles/agent-${PV}"
	dobin bundles/agent-${PV}/bin/hq-agent-nowrapper.sh

	insinto "${HQ_INSTALL_DIR}/bundles/agent-${PV}"
	doins -r bundles/agent-"${PV}"/conf
	doins -r bundles/agent-"${PV}"/lib
	doins -r bundles/agent-"${PV}"/pdk

	fperms 755 "${HQ_INSTALL_DIR}/bundles/agent-${PV}"/pdk/scripts/{device_iostat_all.pl,device_iostat.pl,get_sysmon_stats.pl}

	# install the init.d script
	local initscript="${T}/hq-agent.rc"
	sed -e "s:@@BINDIR@@:${HQ_INSTALL_DIR}/bundles/agent-${PV}/bin:g" \
		-e "s:@@DATA@@:${HQ_INSTALL_DIR}/data/tokendata:g" \
		-e "s:@@USER@@:${HQ_USER}:g" \
		-e "s:@@GROUP@@:${HQ_GROUP}:g" \
		"${FILESDIR}/hq-agent.rc" > "${initscript}" || die
	newinitd "${initscript}" hq-agent

	keepdir "${HQ_INSTALL_DIR}"/data
	keepdir "${HQ_INSTALL_DIR}"/log
	keepdir "${HQ_INSTALL_DIR}/bundles/agent-${PV}/tmp"
	keepdir "${HQ_INSTALL_DIR}/bundles/agent-${PV}/pdk/work"

	fowners "${HQ_USER}:${HQ_GROUP}" "${HQ_INSTALL_DIR}"/{data,log}
	fowners "${HQ_USER}:${HQ_GROUP}" "${HQ_INSTALL_DIR}/bundles/agent-${PV}"/{tmp,pdk/work}
}
