# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit flag-o-matic

DESCRIPTION="SiLK is a collection of traffic analysis tools."
HOMEPAGE="http://tools.netsa.cert.org/silk/index.html"
SRC_URI="http://tools.netsa.cert.org/releases/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="adns ipv6 +python static-libs"

DEPEND="
	dev-libs/lzo
	adns? ( net-libs/adns )
	net-libs/gnutls
	net-libs/libpcap
	sys-libs/zlib
	"
RDEPEND="${DEPEND}"

src_configure() {
	use python && append-ldflags $(no-as-needed)
	econf \
		--enable-data-rootdir=/var/lib/silk \
		$(use_with python) \
		$(use_enable ipv6) \
		$(use_enable static-libs static)
}

src_install() {
	default
	find "${ED}" -name '*.la' -delete
}
