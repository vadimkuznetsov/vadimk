# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"
USE_RUBY="ruby18 ruby19 jruby"

RUBY_FAKEGEM_TASK_DOC=""
RUBY_FAKEGEM_TASK_TEST=""

RUBY_FAKEGEM_EXTRADOC="NEWS README README.rdoc"

inherit ruby-fakegem

DESCRIPTION="ruby-libvirt provides Ruby bindings for libvirt."
HOMEPAGE="http://libvirt.org/ruby/index.html"
#SRC_URI="http://libvirt.org/ruby/download/${P}.tgz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

