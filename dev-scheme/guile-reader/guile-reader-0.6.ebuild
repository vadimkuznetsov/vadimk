# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit autotools eutils git-r3

DESCRIPTION="Guile-reader is a simple framework for building readers for GNU Guile."
HOMEPAGE="http://www.nongnu.org/guile-reader/"
EGIT_REPO_URI="git://git.sv.gnu.org/guile-reader.git"
LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE=""

DEPEND=">=dev-scheme/guile-1.8"
RDEPEND="${DEPEND}"

src_unpack() {
	git-r3_src_unpack
}

src_prepare() {
	eautoreconf -i
}

src_configure() {
	econf --with-guilemoduledir=/usr/share/guile/site
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
}
